package com.example.androidviewflipper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {
	private ViewFlipper mViewFlipper;
	private GestureDetector mGestureDetector;

	int[] resources = { R.drawable.image_one, R.drawable.image_two,
			R.drawable.image_three, R.drawable.image_four,
			R.drawable.image_five, R.drawable.image_six };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get the ViewFlipper
		mViewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

		// Add all the images to the ViewFlipper
		for (int i = 0; i < resources.length; i++) {
			ImageView imageView = new ImageView(this);
			imageView.setImageResource(resources[i]);
			mViewFlipper.addView(imageView);
		}

		// Set in/out flipping animations
		// mViewFlipper.setInAnimation(this, android.R.anim.fade_in);
		// mViewFlipper.setOutAnimation(this, android.R.anim.fade_out);
		/*
		 * set auto start flipping mViewFlipper.setAutoStart(true);
		 * mViewFlipper.setFlipInterval(5000); // flip every 2 seconds (2000ms)
		 */

		CustomGestureDetector customGestureDetector = new CustomGestureDetector();
		mGestureDetector = new GestureDetector(this, customGestureDetector);
	}

	class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			// Swipe left (next)
			if (e1.getX() > e2.getX()) {
				mViewFlipper.setInAnimation(MainActivity.this, R.anim.left_in);
				mViewFlipper
						.setOutAnimation(MainActivity.this, R.anim.left_out);

				mViewFlipper.showNext();
			}

			// Swipe right (previous)
			if (e1.getX() < e2.getX()) {
				mViewFlipper.setInAnimation(MainActivity.this, R.anim.right_in);
				mViewFlipper.setOutAnimation(MainActivity.this,
						R.anim.right_out);

				mViewFlipper.showPrevious();
			}

			return super.onFling(e1, e2, velocityX, velocityY);
		}

		@Override
		public void onShowPress(MotionEvent e) {

			super.onShowPress(e);
			Toast.makeText(getApplicationContext(), "onShowPress", Toast.LENGTH_SHORT).show();
		}

		public void onLongPress(MotionEvent e) {
			Toast.makeText(getApplicationContext(), "onLongPress", Toast.LENGTH_SHORT).show();
			Toast.makeText(getApplicationContext(), "onDown", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(getApplicationContext(),ImageActivity.class);
			startActivity(intent);
		}

		public boolean onDoubleTap(MotionEvent e) {
			Toast.makeText(getApplicationContext(), "onDoubleTap", Toast.LENGTH_SHORT).show();
			return false;
		}

		public boolean onDoubleTapEvent(MotionEvent e) {
			Toast.makeText(getApplicationContext(), "onDoubleTapEvent", Toast.LENGTH_SHORT).show();
			return false;
		}

		public boolean onSingleTapConfirmed(MotionEvent e) {
			Toast.makeText(getApplicationContext(), "onSingleTapConfirmed", Toast.LENGTH_SHORT).show();
			return false;

		}

		public boolean onDown(MotionEvent e) {
			// Must return true to get matching events for this down event.
			
			
			
			return true;
		}

		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				final float distanceX, float distanceY) {
			Toast.makeText(getApplicationContext(), "onScroll", Toast.LENGTH_SHORT).show();
			return super.onScroll(e1, e2, distanceX, distanceY);
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mGestureDetector.onTouchEvent(event);

		return super.onTouchEvent(event);
	}

}